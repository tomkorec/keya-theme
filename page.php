<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/templates/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::context();

$historyQuery = [
	'post_type' => 'historie',
	'posts_per_page' => -1,
	'orderby' => ['title' => 'DESC'],
];

$knihyQuery = [
	'post_type' => 'knihy',
	'posts_per_page' => -1,
	'orderby' => ['title' => 'DESC'],
];

$sticky = get_option('sticky_posts');

$stickyPostQuery = [
	'post_type' => 'post',
	'posts_per_page' => 1,
	'post__in' => $sticky,
	'ignore_sticky_posts' => 1
];

$newsQuery = [
	'post_type' => 'post',
	'posts_per_page' => -1,
	'post__not_in' => $sticky,
	'category_name' => 'aktuality'
];

if (function_exists('yoast_breadcrumb')) {
	$context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="breadcrumbs">', '</nav>', false);
}

$timber_post = new Timber\Post();
$context['post'] = $timber_post;
$context['history'] = Timber::get_posts($historyQuery);
$context['knihy'] = Timber::get_posts($knihyQuery);
$context['news'] = Timber::get_posts($newsQuery);
$context['sticky'] = isset($sticky[0]) ? Timber::get_posts($sticky)[0] : null;
Timber::render(array('page-' . $timber_post->post_name . '.twig', 'page.twig'), $context);
