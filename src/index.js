/* I. styles import */
import './styles/main.styl';
import '@glidejs/glide/src/assets/sass/glide.core.scss';

import SmoothScroll from 'smooth-scroll';
import Glide from '@glidejs/glide'
import {Main} from "./typescript/Main";

import $ from 'jquery';


new Main();

let scrollOffset = 210;


if (isCurrentPage('history')) {
	const scroll = new SmoothScroll('a[href*="#"]', {
		offset: function () {
			if (screen.width > 769) {
				return scrollOffset;
			} else {
				return 40;
			}
		}
	});
}

let options = {
	perView: 3,
	gap: 20,
	breakpoints: {
		960: {
			perView: 2,
		},
		768: {
			perView: 1
		}
	}
};

if (isCurrentPage('homepage')) {
	new Glide('.glide', options)
		.mount();

	const scroll = new SmoothScroll('a[href*="#"]', {
		offset: function () {
			if (screen.width > 769) {
				return 100;
			} else {
				return 60;
			}
		}
	});
}

$('.construction-close').click(e => {
	$('.construction').slideUp();
});


function isCurrentPage(pageName) {
	return !!document.querySelector('[data-name="' + pageName + '"]');
}
