import {Header} from "./Header";
import {MobileMenu} from "./MobileMenu";

export class Main {

	constructor() {
		new Header();
		new MobileMenu();

	}

	private static isCurrentPage(pageName: string): boolean {
		return !!document.querySelector('[data-name="' + pageName + '"]');
	}

}
