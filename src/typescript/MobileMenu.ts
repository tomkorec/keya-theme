export class MobileMenu {
	private mobileMenu: HTMLElement;
	private hamburger: HTMLElement;
	private parentItems: NodeListOf<HTMLElement>;

	constructor() {
		this.mobileMenu = document.querySelector('.navbar-mobile');
		this.hamburger = document.querySelector('.hamburger');
		this.parentItems = document.querySelectorAll('.menu-item-has-children');

		this.bindEventListeners();
	}


	private bindEventListeners() {
		this.hamburger.addEventListener('click', () => {
			this.mobileMenu.classList.toggle('active');
			this.hamburger.classList.toggle('active');
		});

		this.parentItems.forEach(parent => {
			let parentLink = parent.querySelector(':scope > a');
			let children = parent.querySelector(':scope > .navbar-mobile-children');
			parentLink.addEventListener('click', () => {
				parentLink.classList.toggle('active');
				children.classList.toggle('active');
			});
		});
	}
}
