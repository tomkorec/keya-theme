import {fromEvent, Subject} from "rxjs";
import {debounceTime, tap, throttleTime} from "rxjs/operators";

export class Header {
	private header: HTMLElement;
	private scrollSubject = new Subject<number>();

	constructor() {
		this.header = document.querySelector('header.header');

		this.bindEventListeners();
	}

	public bindEventListeners() {
		fromEvent(window, 'scroll')
			.pipe(
				// debounceTime(300),
			)
			.subscribe(() => {
				if (window.scrollY > 0) {
					this.header.classList.add('scrolling');
				} else {
					this.header.classList.remove('scrolling');
				}
			});

	}

}
