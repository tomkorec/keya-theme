<?php


class Tomato
{
	function getAlbumByName($name, $getOnlyName = false)
	{
		return [];
		$url = 'https://skautikeya.rajce.idnes.cz/' . $name . '?rss=media';
		$response = @file_get_contents($url);
		if (false === $response) {
			return [];
		}
		$itemsArray = array();
		$xml = new SimpleXMLElement($response);
		$albumTitle = $xml->xpath('.//channel/title')[0];
		preg_match('/›\s*([^›]*)$/', $albumTitle, $albumTitle);
		if ($getOnlyName) {
			return $albumTitle[1];
		} else {
			$items = $xml->xpath('.//channel/item');
			foreach ($items as $item)
				return array('title' => $albumTitle[1], 'items' => $itemsArray);

		}
	}

	function getFeedFromUrl($url)
	{
		$itemsArray = array();
		$response = @file_get_contents($url);
		if (false === $response) {
			return [];
		}
		$xml = new SimpleXMLElement($response);
		$items = $xml->xpath('.//channel/item');
		foreach ($items as $item) {
			$title = $this->fixTitle($item->xpath('.//title')[0]);
			$img = $item->xpath('.//image/url')[0];
			$link = $item->xpath('.//image/link')[0];

//            echo $title.'<br>';
//            echo $img.'<br>';
//            echo $link.'<br>';
			$urlName = preg_replace('/\s+/', '_', $title);
			$current = array(
				'title' => $title,
				'urlName' => $urlName,
				'	img' => $img,
				'link' => $link
			);
			array_push($itemsArray, $current);

		}
		return $itemsArray;
	}

	function fixTitle($orig)
	{
		preg_match('/[^\|]*\|\s{1}/', $orig, $prefix);
		$pos = strlen($prefix[0]);
		return substr($orig, $pos);
	}


}
